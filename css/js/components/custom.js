$(function () {
    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }
    $('#pagecontent').find('.visual').insertAfter($('.navbar')); // set visual to visual content in header


    function filterMenuCollapse() {
        var $accordion = $('.tag-group-ul');

        $('.tag-group-ul').each(function () {
            if ($(this).has('ul')) {

                $(this).has('ul').find('ul').addClass('show');

                var _self = $(this).has('ul');
                var openAccordion = _self.find('h3');
                var accordionBody = _self.find('.tag-group-ul li ul');

                $(accordionBody).addClass('show');

                $('<i class="icon"></i>').appendTo(openAccordion);

                if (accordionBody.hasClass('hide')) {
                    _self.find('h3').removeClass('active');
                    openAccordion.find('.icon').addClass('icon-chevron-right');
                } else {
                    _self.find('h3').addClass('active');
                    openAccordion.find('.icon').addClass('icon-chevron-down');
                }
            }
        });

        $('.tag-group-ul').has('ul').find('h3').on('click', function (e) {
            e.preventDefault();
            var _self = $(this);
            var target = _self.closest($accordion).find('ul');
            var icon = _self.find('.icon');

            target.toggleClass('hide show');

            if (target.is(":visible")) {
                _self.addClass('active');
                icon.removeClass('icon-chevron-right').addClass('icon-chevron-down');
            } else {
                _self.removeClass('active');
                icon.removeClass('icon-chevron-down').addClass('icon-chevron-right');
            }

        });

    };

    filterMenuCollapse();

});