$(function () {

    var parallax = $('.component__parallax');

    var doParallax = function(){

        var viewportHeight = $(window).height();
        var documentScroll = $(document).scrollTop();

        parallax.each(function(){
            var parallaxHeight = $(this).height();
            var parallaxCenter = $(this).offset().top + 0.5*$(this).height();
            var currentPosition = (parallaxCenter - documentScroll) / viewportHeight;

            var parallaxImages = $(parallax).children('div');

            
            $(parallaxImages).each(function(){

                var imageSpeed = $(this).data('parallax-speed');
                var imageHorizontal = $(this).data('parallax-horizontal-position') ? $(this).data('parallax-horizontal-position') : 50;
                var imageVertical = $(this).data('parallax-vertical-position') ? $(this).data('parallax-vertical-position')+'%' : 0;
                var imageSize= $(this).data('parallax-size') ? $(this).data('parallax-size')+'%' : 100 + imageSpeed;

                $(this).css({
                    top: (currentPosition - 1) * (parallaxHeight * imageSpeed / 100),
                    height: (parallaxHeight * (120 + imageSpeed)/100)
                });

            });

        });
    }


    if(parallax.length > 0){
        $(parallax).each(function(){
            var parallaxImages = $(parallax).children('div');
            doParallax();
            $(parallaxImages).each(function(){
                var imageSrc = $(this).data('parallax-imgsrc');
                var imageIndex = $(this).data('parallax-index');
                var imageBlur = $(this).data('parallax-blur') ? $(this).data('parallax-blur') : 0;
                var imageOpacity = $(this).data('parallax-opacity') ? $(this).data('parallax-opacity') : 1;
                var imageSize = $(this).data('parallax-size') ? $(this).data('parallax-size') : 1;

                $(this).css({
                    backgroundImage: 'url('+imageSrc+')',
                    zIndex: imageIndex,
                    opacity: imageOpacity,
                });
                if(imageBlur > 0){
                    $(this).css({
                        filter: 'blur('+imageBlur+'px)'
                    });
                }
                if(imageSize != 1){
                    $(this).css({
                        'transform': 'scale('+imageSize+')'
                    });

                }

                $(this).fadeIn();

            });

        });
    }

    var parallaxing = false;

    $(window).scroll(function () {
        doParallax();

        // if (parallaxing === false){
        //     doParallax();
        //     parallaxing = true;
        //     setTimeout(function(){
        //         parallaxing = false;
        //     },150);
        // }
    });
});
